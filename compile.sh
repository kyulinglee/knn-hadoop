#!/bin/sh
current_path=$(pwd)
#rm *.jar
javac -classpath /usr/local/hadoop/*:/usr/local/hadoop/lib/* *.java
jar cvf Driver.jar *.class
rm *.class
hadoop jar Driver.jar Driver $current_path/input_classified_data.txt virginica $current_path/iris_training_dataset.txt $current_path

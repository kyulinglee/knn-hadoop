import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// hadoop 클래스 선언
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Driver {
	public static void main(String[] args) throws IOException, InterruptedException, ClassNotFoundException {
		int num_features=0;

		System.out.println("arg 0:"+args[1]);

		Configuration conf=new Configuration();
		FileSystem hdfs=FileSystem.get(conf);

		//args[0]는 분류괴디를 기다리는 입력의 특징을 가진 파일 경로를 지정.
		BufferedReader br = new BufferedReader(new InputStreamReader(hdfs.open(new Path(args[0]))));
		String line=null;
		while((line=br.readLine())!=null){
			String[] feat=line.toString().split("\\ ");
			for(int i=0;i<feat.length;i++)
				conf.setFloat("feat"+i, Float.parseFloat(feat[i]));
			num_features=feat.length;
			break;
		}
		br.close();
		hdfs.close();
		conf.setInt("num_features",num_features);

		//args[1]는 정의된 이름을 받아온다.
		conf.set("name",args[1]);
		Job job = new Job(conf,"KNN Classification MapReduce");
		job.setJarByClass(Driver.class);

		//args[2]는 입력에서 학습에 사용할 파일을 가져온다.
		FileInputFormat.setInputPaths(job, new Path(args[2]));

		//args[3]는 출력 파일의 경로를 가져온다.
		FileOutputFormat.setOutputPath(job, new Path(args[3]));
		job.setMapperClass(Map.class);
		
		//job.setCombinerClass(Reduce.class);
		job.setReducerClass(Reduce.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		job.waitForCompletion(true);
	}
}
